# Setting up a Raspberry Pi over the network

Note: this tutorial assumes you're a proficient user with access to a Linux computer (or Windows with a WSL command line). If that's not the case, I recommend you ask Google for "installing a Raspberry Pi without a screen".

## Requirements

Hardware:
- Raspberry Pi (version 2 or higher with ethernet port)
- Micro-SD card with adapter
- A modem with Ethernet port


Software:
- Raspberry Pi installer ([download here](https://www.raspberrypi.org/software/))

##  Creating the image

Plug the micro-SD card in the computer, open the *Raspberry Pi Installer* and follow the instructions (the tutorial below is meant for Raspbian, and if you're looking for a minimal install, it is recommended to use Raspbian Lite).

When the installation is complete, open the *boot* partition and add an empty *SSH* file (without any extension - this will enable SSH on the Raspberry Pi) in the root folder of the partition.

Remove the micro-SD card from the computer and insert it in the Raspberry Pi.

## Setup over the network

Connect the Raspberry Pi to your modem using an ethernet cable, and plug the power supply into the Raspberry Pi. Give it a minute to boot before proceeding to the next step.

### Locating the Pi on the network

Identify the IP address of the Pi using the NMAP tool in the command line:
~~~
nmap -sn 192.168.*.*
~~~

NOTE: If you already know which subnet your Pi is connected to, you can speed this up (a lot) by specifying it more precisely, for example:
~~~
nmap -sn 192.168.8.*
~~~

### Connecting to the Pi via SSH

Connect to the Pi via SSH by opening a terminal and executing (replace x.x by the actual IP of your Pi):
~~~
ssh pi@192.168.x.x
~~~

The standard password is "*raspberry*".

You are now connected to the Pi. Make sure to change the password straight away by updating the password (otherwise anyone will be able to access the Pi using these standard credentials) using the following command.

~~~
passwd
~~~

Your Pi is now ready for usage. For best up to date behaviour, remember to update and upgrade the package manager:

~~~
sudo apt-get update
sudo apt-get upgrade
~~~

## Installing and using an updated version of Python on the Pi


### Installation
The Python distributions available in the Raspbian repository are typically outdated. You can install later versions by running (replace 3.8.5 by whichever distribution you'd like to install):

~~~
sudo apt-get install -y build-essential tk-dev libncurses5-dev libncursesw5-dev \
libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev \ libexpat1-dev liblzma-dev zlib1g-dev libffi-dev

version=3.8.11

wget https://www.python.org/ftp/python/$version/Python-$version.tgz

tar zxf Python-$version.tgz
cd Python-$version
./configure --enable-optimizations
make -j4
sudo make altinstall
~~~

This will take a while (it's actually more recommended and comfortable to pack these lines into a shell file and let it run all at once in the background - such a shell file is provided in *install_newer_python_on_rpi.sh* ).


### Usage
The original version of Python will still be linked in the path and called with *python3*. In order to specifically call this Python version, you will need to call *python3.x* (where x is the first version number after 3). A similar postfixing will be required for *pip*.

If you create [virtual environments](https://docs.python.org/3/tutorial/venv.html) with this version, using *python3* and *pip3* in these will however be sufficient once these are activated. Some more tips on enviroment variables and Python environments are found [here](https://pybit.es/persistent-environment-variables.html).


## Installing Docker on the Pi

Run the following commands:
~~~
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
~~~

And then add the *pi* (Raspbian default) user to the list of users allowed to run containers:

~~~
sudo usermod -aG docker pi
~~~

Further tips on install/delete are found [here](https://phoenixnap.com/kb/docker-on-raspberry-pi).
