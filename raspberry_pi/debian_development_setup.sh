#!/bin/bash

# -----------------------------------------------------
# Options:
CODE_FOLDER_PATH=~/code
VENV_NAME=venv
# -----------------------------------------------------

echo "------- SETTING UP THE PACKAGES -----------"
# Installing typical requirements and creating keys
sudo apt-get install git
ssh-keygen -t rsa

# Configuring the line breaks
git config --global core.autocrlf input

echo "------- CREATING THE VIRTUAL ENVIRONMENT -----------"
cd "$CODE_FOLDER_PATH"

python3.8 -m venv "$VENV_NAME/" &&
source "$VENV_NAME/bin/activate" &&

python3.8 -m pip install --upgrade pip 


echo "------- CLONING THE GIT REPOSITORIES -----------"
mkdir seedftw 
git clone https://gitlab.com/Pierre_VF/seedftw.git seedftw

mkdir senasopt
git clone https://gitlab.com/Pierre_VF/senasopt.git senasopt

mkdir plotneat
git clone https://gitlab.com/Pierre_VF/plotneat.git plotneat

echo "-------- Installing packages in development mode --------"
pip install -e seedftw/.[dev]
pip install -e senasopt/.[dev]
pip install -e plotneat/.

echo "-----------------[ DONE :-) ]----------------------------"
