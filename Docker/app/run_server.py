from fastapi import FastAPI
import uvicorn

app = FastAPI(name="Hello world!")


@app.get("/")
def _index():
    return {"Hello": "you"}


if __name__ == "__main__":
    print("The container is starting !")
    uvicorn.run(app, host="0.0.0.0", port=8001)
