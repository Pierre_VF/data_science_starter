# Getting started with Python

Below, instructions to get started with Python.

----
## Setting up the workspace

### Miniconda installation 

Go to https://docs.conda.io/en/latest/miniconda.html and download the latest version.

On Linux, this can be done with (first line to download, second to checksum):
~~~
wget [URL to download from]
sha256sum [filename]
~~~



### Conda environment creation

Make sure to create a dedicated environment for developments:
~~~
conda create --name [environment-name] python=3.8
~~~

Which can then be activated with:
~~~
conda activate [environment-name]
~~~

Add conda-forge as a channel:
~~~
conda config --add channels conda-forge
~~~

Register the environment with IPython:
~~~
python3 -m pip install ipykernel
python3 -m ipykernel install --user
~~~

Deactivate environment with:
~~~
conda deactivate
~~~

More details on environments [in the doc](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html).

### Jupyter lab

Install Jupyter Lab:
~~~
conda install jupyterlab
~~~

Install Node.js:
~~~
conda install nodejs
~~~

Install the table of contents plug-in:
~~~
jupyter labextension install @jupyterlab/toc
~~~

### Plotly and Dash for visualisation

Install Plotly:
~~~
conda install plotly
~~~

And the widgets for Jupyterlab:
~~~
jupyter labextension install jupyterlab-plotly
jupyter labextension install @jupyter-widgets/jupyterlab-manager plotlywidget
~~~

For web-server based development, install:
~~~
conda install dash
~~~

----
## Development

### Installation of packages in edit mode

Install a package in development mode:
~~~
cd [folder/of/package/with/setup.py]
pip install -e .
~~~

----
## Version control

### Version control of notebooks

To avoid issues with residual data in Git, the notebooks should be stripped of their outputs before committing.

This can be done with *nbstripout*, which is installed with:
~~~
conda install -c conda-forge nbstripout
~~~

Then, add a Git clean filter with the following content:
~~~
nbstripout --install --attributes .gitattributes
~~~

Further details [here](http://mateos.io/blog/jupyter-notebook-in-git/) and [there](http://timstaley.co.uk/posts/making-git-and-jupyter-notebooks-play-nice/).


