# Tips and tricks to get started on Google Cloud Platform

This notebook contains an introduction to application deployment on GCP.

-----
## App engine

### Creation of project

Update tool and install the gcloud component for the App Engine extension for Python 3:
~~~
gcloud components update
gcloud components install app-engine-python
~~~

Create project (with name *[project-name]* )
~~~
gcloud projects create [project-name] --set-as-default
~~~

Verify creation of the project
~~~
gcloud projects describe [project-name]
~~~


Create the project engine (with region *europe-west*)
~~~
gcloud app create --project=[project-name]
~~~


### Code structure for an app

The code of an app must contain the following files in its root folder (with exact names):
- *main.py* : contains the code of the app itself
- *requirements.txt* : contains the specification of the python packages to be installed (one per line)
- *.gcloudignore* : equivalent of .gitignore (same syntax) for ignoring files in cloud upload (e.g. environment)
- *app.yaml* : YAML file describing the setup of the app (see details below)


The *app.yaml* file has following structure (more details [here](https://cloud.google.com/appengine/docs/standard/python3/config/appref)):
~~~
runtime: python38

env_variables:
  PORT: 8080
  
entrypoint: 
    gunicorn -b :$PORT main:app
~~~

Where *main:app* indicates that the file containing the app is *main* and the server function within the file is *app*.
Note that all apps on GCP use port 8080.
More details on *gunicorn* can be found [here](https://docs.gunicorn.org/en/stable/run.html).

### Local test of project

Start by updating pip:
~~~
python -m pip install --upgrade pip
~~~

Execute the following to test the environment:
~~~
cd [location/of/the/app]
python -m venv env
env\Scripts\activate
pip install -r requirements.txt
pip install .
~~~

And then run the app:
~~~
python main.py
~~~

If everything runs well, then you're ready to deploy.

### Deploying to the cloud

Make sure to be in the folder of the project on your local machine:
~~~
cd [location/of/the/app]
~~~

Deploy to the cloud with:
~~~
gcloud app deploy
~~~


View app in browser (url format: https://PROJECT_ID.REGION_ID.r.appspot.com )
~~~
gcloud app browse
~~~

----
## Further reading

Some hints online:
- Basic tutorial for apps [here](https://cloud.google.com/appengine/docs/standard/python3/quickstart)


