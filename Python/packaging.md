# Packaging and distribution of Python code

Below, instructions for packaging and open-source Python development.

## Creating a distribution of the source code

First, make sure that [setuptools](https://setuptools.readthedocs.io/) is installed:
~~~
conda install setuptools
~~~


A package distribution can be created with setuptools using the command:

~~~
cd [location/of/the/package]
python setup.py sdist
~~~

Warning: before proceeding, ensure that the project's configuration reflects the configuration of the package.

## Publishing the package to PyPi

Create a user and package repository on [PyPi](https://pypi.org/).
Then make sure that *twine' is installed:
~~~
conda install twine
~~~

Then make sure to create a distribution of the source code accordingly with the previous section.

Once the source distribution is created, it can be uploaded with the following command (in the directory of the package):
~~~
cd [location/of/the/package]
twine upload dist/*
~~~

More details [here](https://medium.com/@joel.barmettler/how-to-upload-your-python-package-to-pypi-65edc5fe9c56)


```python

```
