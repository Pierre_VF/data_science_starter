

# Configuring SSH key access

First, add your public key to your Github profile [here](https://github.com/settings/keys)

Add the SSH key from the Github server:

    ssh -T git@github.com

Add the origin as an SSH remote:

    git remote set-url origin git@github.com:[-- username/your-repository.git --]


For more detail, see [here](https://gist.github.com/xirixiz/b6b0c6f4917ce17a90e00f9b60566278)
